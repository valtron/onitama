import json
import asyncio
from typing import Dict
from aiohttp import web, WSMsgType

import onitama

def main() -> None:
	loop = asyncio.get_event_loop()
	loop.create_task(_windows_ctrl_c_workaround())
	app = create_app(loop)
	web.run_app(app, host = '0.0.0.0', port = 4008)

def create_app(loop: asyncio.AbstractEventLoop) -> web.Application:
	import jinja2
	
	app = web.Application(loop = loop)
	app['jinja'] = jinja2.Environment(
		loader = jinja2.FileSystemLoader('tmpl'),
		autoescape = jinja2.select_autoescape(default = True),
	)
	
	app.router.add_get('/', page_home)
	app.router.add_get('/ws/seed/{seed}', page_websocket)
	app.router.add_static('/static', path = 'static', name = 'static')
	return app

async def page_home(req: web.Request) -> web.Response:
	tmpl = req.app['jinja'].get_template('home.html')
	return web.Response(
		status = 200, content_type = 'text/html',
		text = tmpl.render(title = "Onitama"),
	)

async def page_websocket(req: web.Request) -> web.Response:
	from agents import BFSAgent
	from test_trained import get_trained_heuristic
	
	seed = int(req.match_info['seed'])
	
	ws = web.WebSocketResponse()
	await ws.prepare(req)
	
	env = onitama.OnitamaEnv()
	env.seed(seed)
	env.reset()
	opponent = BFSAgent(depth = 3, heuristic = get_trained_heuristic('weights-3074.pth'))
	
	if env.state.turn != 0:
		env.step(opponent.act(env))
	
	actions = env.state.build_actions()
	await ws.send_str(serialize_state(env, actions))
	
	async for msg in ws:
		if msg.type == WSMsgType.ERROR:
			print("ws connection closed with exception {}".format(ws.exception()))
			continue
		
		assert env.state.turn == 0
		data = json.loads(msg.data)
		_, _, done, _ = env.step(actions[data['actionIndex']])
		if done: break
		_, _, done, _ = env.step(opponent.act(env))
		if done: break
		actions = env.state.build_actions()
		await ws.send_str(serialize_state(env, actions))
	
	await ws.send_str(serialize_state(env, []))
	
	return ws

def serialize_state(env, actions):
	state = env.state
	return json.dumps({
		'my_turn': 0,
		'state': {
			'turn': state.turn,
			'cards_by_player': state.cards_by_player,
			'card_index_to_swap': state.card_index_to_swap,
			'board': [
				[serialize_cell(cell) for cell in row]
				for row in state.board
			],
		},
		'actions': actions,
	})

def serialize_cell(cell):
	if not cell: return None
	return {
		'player': (0 if cell & onitama.PLAYER_MARK[0] else 1),
		'master': bool(cell & onitama.MASTER_MARK),
	}

async def _windows_ctrl_c_workaround() -> None:
	import os
	if os.name != 'nt': return
	# https://bugs.python.org/issue23057
	while True:
		await asyncio.sleep(0.1)

if __name__ == '__main__':
	main()
