class OnitamaGame {
	constructor($form, $game) {
		$form.addEventListener('submit', this._onFormSubmit.bind(this));
		this.$game = $game;
		this.my_turn = null;
		this.state = null;
		this.actions = null;
		this.ws = null;
		this._selected_piece_id = null;
		this._hovered_piece_id = null;
		this._createBoard();
	}
	_onFormSubmit(evt) {
		const $input = evt.target.seed;
		const seed = $input.value;
		const loc = window.location;
		const isHTTPS = (loc.protocol == 'https:');
		const wsurl = 'ws' + (isHTTPS ? 's' : '') + '://' + loc.host + '/ws/seed/' + seed;
		
		if (this.ws) {
			this.ws.onmessage = null;
			this.ws.close();
		}
		this.ws = new WebSocket(wsurl);
		this.ws.onmessage = this._onWSMessage.bind(this);
		this._showMessage("Starting...");
		
		evt.preventDefault();
		return false;
	}
	_onWSMessage(evt) {
		this.refresh(JSON.parse(evt.data));
	}
	_createBoard() {
		const $board = this.$game.querySelector('.board');
		for (let y = 4; y >= 0; --y) {
			for (let x = 0; x < 5; ++x) {
				const $cell = document.createElement('div');
				$cell.classList.add('cell', 'cell-' + x + '_' + y, 'cell-' + ((x+y)%2));
				const $pieceContainer = document.createElement('div');
				$pieceContainer.classList.add('cell-piece');
				$cell.appendChild($pieceContainer);
				const $moveContainer = document.createElement('div');
				$moveContainer.classList.add('cell-moves');
				$cell.appendChild($moveContainer);
				$board.appendChild($cell);
			}
		}
		const $cards = this.$game.querySelector('.cards');
		for (let i = 0; i < 4; ++i) {
			const $card = document.createElement('div');
			$card.classList.add('card', 'p' + Math.floor(i / 2));
			for (let y = 4; y >= 0; --y) {
				for (let x = 0; x < 5; ++x) {
					const $cell = document.createElement('div');
					$cell.classList.add('card-cell');
					$cell.classList.add('card-cell-' + x + '_' + y);
					if (x == 2 && y == 2) {
						$cell.classList.add('card-cell-anchor');
					}
					$card.appendChild($cell);
				}
			}
			
			if (i < 2) {
				$card.addEventListener('click', this._moveClick.bind(this, $card));
			}
			
			const $cardwrap = document.createElement('div');
			$cardwrap.classList.add('card-wrap');
			$cardwrap.appendChild($card);
			$cards.appendChild($cardwrap);
		}
	}
	refresh(data) {
		console.log(data);
		if (data.error) {
			alert(data.message);
			return;
		}
		
		this.my_turn = data.my_turn;
		
		const state = data.state;
		this.state = state;
		
		const actions = data.actions;
		this.actions = actions;
		
		for (const $cell of this.$game.querySelectorAll('.board .cell')) {
			$cell.classList.remove('p0', 'p1', 'has-moves', 'highlight');
			$cell.querySelector('.cell-piece').textContent = '';
		}
		
		for (let y = 0; y < 5; ++y) {
			for (let x = 0; x < 5; ++x) {
				const piece = state.board[x][y];
				if (!piece) continue;
				const $cell = this._boardCell(y, x);
				$cell.classList.add('p' + piece.player);
				$cell.querySelector('.cell-piece').textContent = PIECECHAR[piece.master ? 'king' : 'pawn'];
			}
		}
		
		for (const $cell of this.$game.querySelectorAll('.card-cell')) {
			$cell.classList.remove('card-cell-move');
		}
		const $cards = this.$game.querySelectorAll('.card');
		for (let turn = 0; turn < 2; ++turn) {
			for (let i = 0; i < 2; ++i) {
				const $card = $cards[turn * 2 + i];
				$card.classList.remove('card-to-swap', 'has-moves', 'move', 'highlight');
				for (const move of state.cards_by_player[turn][i][this.my_turn]) {
					const $cell = this._cardCell($card, move[0], move[1]);
					$cell.classList.add('card-cell-move');
				}
			}
		}
		if (state.card_index_to_swap != null) {
			$cards[2 + state.card_index_to_swap].classList.add('card-to-swap');
		}
		
		for (const $move of this.$game.querySelectorAll('.cell .move')) {
			$move.remove();
		}
		
		let i = 0;
		for (const action of actions) {
			const cardIndex = action[0];
			const moveIndex = action[1];
			const $card = $cards[0 + cardIndex];
			
			let $move;
			if (moveIndex == null) {
				$card.classList.add('has-moves', 'move');
				$move = $card;
			} else {
				const x1 = action[2];
				const y1 = action[3];
				const [dx, dy] = state.cards_by_player[this.my_turn][cardIndex][this.my_turn][moveIndex];
				const x2 = x1 + dx;
				const y2 = y1 + dy;
				
				$move = document.createElement('div');
				$move.classList.add('move');
				$move.dataset.cardIndex = cardIndex;
				$move.dataset.x1 = x1;
				$move.dataset.y1 = y1;
				const $cell1 = this._boardCell(x1, y1);
				const $cell2 = this._boardCell(x2, y2);
				$move.addEventListener('mouseover', this._moveOver.bind(this, $move, $cell1, $card));
				$move.addEventListener('mouseout' , this._moveOut.bind(this, $move, $cell1, $card));
				$move.addEventListener('click'    , this._moveClick.bind(this, $move));
				$cell2.querySelector('.cell-moves').appendChild($move);
				$cell2.classList.add('has-moves');
			}
			$move.dataset.actionIndex = i;
			
			i += 1;
		}
		
		if (state.turn == null) {
			this._showMessage("Game over.");
		} else if (state.turn == this.my_turn) {
			this._showMessage("Your turn.");
		} else {
			this._showMessage("Waiting for other player...");
		}
	}
	_boardCell(x, y) {
		return this.$game.querySelector('.board .cell.cell-' + x + '_' + y);
	}
	_cardCell($card, x, y) {
		return $card.querySelector('.card-cell.card-cell-' + (x + 2) + '_' + (y + 2));
	}
	_moveOver($move, $cell, $card) {
		$cell.classList.add('highlight');
		$card.classList.add('highlight');
	}
	_moveOut($move, $cell, $card) {
		$cell.classList.remove('highlight');
		$card.classList.remove('highlight');
	}
	_moveClick($obj) {
		if (!$obj.classList.contains('move')) return;
		if ($obj.dataset.actionIndex == null) return;
		const actionIndex = parseInt($obj.dataset.actionIndex);
		this.ws.send(JSON.stringify({
			actionIndex: actionIndex,
		}));
	}
	_cellClick($cell) {
		const gamestate = this.gamestate;
		if (gamestate == null) return;
		if (!gamestate.my_turn) return;
		const piece = this._cellPiece($cell);
		
		if (this._selected_piece_id != null) {
			if ($cell.classList.contains('move-option')) {
				this.ws.send(JSON.stringify({
					piece: this._selected_piece_id,
					x: parseInt($cell.dataset.x),
					y: parseInt($cell.dataset.y)
				}));
				this._selected_piece_id = null;
			} else if (piece) {
				if (piece.player == gamestate.me) {
					this._selected_piece_id = piece.id;
				} else {
					this._selected_piece_id = null;
				}
			} else {
				this._selected_piece_id = null;
			}
		} else if (piece && piece.player == gamestate.me) {
			this._selected_piece_id = piece.id;
		} else {
			this._selected_piece_id = null;
		}
		
		this._updateBoard();
	}
	_showMessage(text) {
		this.$game.querySelector('.message').textContent = text;
	}
}

const PIECECHAR = {
	king: String.fromCharCode(0x265A),
	pawn: String.fromCharCode(0x265F)
};
