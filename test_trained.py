from scipy.special import expit
import torch

from onitama import CARDS, SIZE, PLAYER_MARK, MASTER_MARK, MIDDLE, OnitamaEnv
from agents import RandomAgent, BFSAgent, pawn_heuristic, TreeAgent
import utils, train

def main():
	th = get_trained_heuristic('weights-3074.pth')
	
	# trained_heuristic vs. pawn_heuristic: >300 ELO
	ags = [
		BFSAgent(depth = 1, heuristic = pawn_heuristic),
		BFSAgent(depth = 1, heuristic = th),
	]
	
	env = OnitamaEnv()
	wins = [0, 0]
	
	SEED_OFFSET = 100000000
	
	for i in range(1000):
		env.seed(SEED_OFFSET + i)
		env.reset()
		
		for _ in range(100):
			turn = env.state.turn
			action = ags[turn].act(env)
			assert action is not None
			_, _, done, _ = env.step(action)
			if done: break
		
		rewards = [0, 0]
		if done:
			rewards[turn] += 1
			wins[turn] += 1
		
		print("Game {:4}, ply {:3}: {} /  {:.0f}".format(i, env.ply, rewards, utils.p_to_elo_diff(wins[1] / (wins[0] + wins[1]))))
	
	for i in range(len(wins)):
		print("Player {}: {:4}".format(i, wins[i]))
	print("Delta Elo: {:.0f}".format(utils.p_to_elo_diff(wins[1] / (wins[0] + wins[1]))))

def get_trained_heuristic(weights):
	model = train.StateValueModel(card_embedding_size = 32, board_embedding_size = 32)
	model.load_state_dict(torch.load(weights))
	device = torch.device('cpu')
	model.to(device)
	return trained_heuristic(model, device)

class trained_heuristic:
	def __init__(self, model, device):
		self.model = model
		self.device = device
	
	def __call__(self, state, env):
		value_logit = self.model(*(x.to(self.device) for x in train.create_batch([state]))).item()
		return expit(value_logit)

if __name__ == '__main__':
	main()
