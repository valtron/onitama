import numpy as np
from scipy.special import expit
import torch
from torch import nn, optim
from torch.nn import functional as F
from torch.utils.data import DataLoader

from onitama import CARDS, SIZE, PLAYER_MARK, MASTER_MARK, MIDDLE, OnitamaEnv

def main():
	train_device = torch.device('cuda')
	eval_device = torch.device('cpu')
	
	model = StateValueModel(card_embedding_size = 32, board_embedding_size = 32)
	model.load_state_dict(torch.load('weights-3074.pth'))
	
	history = []
	agent = ModelAgent(model, eval_device, history = history)
	env = OnitamaEnv()
	
	print("Doing rollouts...")
	model.to(eval_device)
	
	for seed in range(3075, 20000):
		env.seed(seed)
		env.reset()
		rollout(env, agent)
		
		if len(history) > 15000:
			print("Training on {} plies (next seed: {})...".format(len(history), seed + 1))
			model.to(train_device)
			train_on_history(model, train_device, history)
			history.clear()
			
			if seed > 4000:
				torch.save(model.state_dict(), 'weights-{}.pth'.format(seed))
				return
			
			print("Doing rollouts...")
			model.to(eval_device)

def train_on_history(model, device, history):
	opt = optim.Adam(model.parameters())
	
	ds = [
		encode_state(state) + (np.float32(value),)
		for state, value in history
	]
	dl = DataLoader(ds, batch_size = 128, shuffle = True)
	for epoch_start in range(10):
		ls = 0
		ln = 0
		print("Epoch {}: ".format(epoch_start + 1), end = '')
		for i, (board, mc0, mc1, uc, oc0, oc1, value) in enumerate(dl):
			n = board.shape[0]
			
			pred = model(board.to(device), mc0.to(device), mc1.to(device), uc.to(device), oc0.to(device), oc1.to(device))
			l = F.binary_cross_entropy_with_logits(pred, value.to(device))
			opt.zero_grad()
			l.backward()
			opt.step()
			
			ls += n * l.item()
			ln += n
		print("{}".format(ls / ln))

def rollout(env, agent):
	for _ in range(1000):
		turn = env.state.turn
		action = agent.act(env)
		assert action is not None
		_, _, done, _ = env.step(action)
		if done: break

def encode_state(state):
	turn = state.turn
	other_turn = 1 - turn
	
	board = state.board
	if turn == 1:
		# Standardize so current player (turn) is in the red position (0)
		board = board[::-1,::-1]
	
	cbp = state.cards_by_player
	c2s = state.card_index_to_swap
	card_swap = (None if c2s is None else cbp[other_turn][c2s])
	
	mc0 = encode_card(cbp[turn][0], card_swap)
	mc1 = encode_card(cbp[turn][1], card_swap)
	csw = encode_card(card_swap, None)
	oc0 = encode_card(cbp[other_turn][0], card_swap)
	oc1 = encode_card(cbp[other_turn][1], card_swap)
	
	enc_board = np.zeros((6, SIZE, SIZE), dtype = np.float32)
	enc_board[0] = 1
	enc_board[1, 0,MIDDLE] =  1
	enc_board[1,-1,MIDDLE] = -1
	enc_board[2] = board & PLAYER_MARK[turn]
	enc_board[3] = board == (PLAYER_MARK[turn] | MASTER_MARK)
	enc_board[4] = board & PLAYER_MARK[other_turn]
	enc_board[5] = board == (PLAYER_MARK[other_turn] | MASTER_MARK)
	
	return (
		enc_board,
		mc0, mc1,
		csw,
		oc0, oc1,
	)

def encode_card(card, card_swap):
	if card is card_swap: return 0
	return 1+CARDS.index(card)

class ModelAgent:
	def __init__(self, model, device, *, history = None):
		self.model = model
		self.device = device
		self.history = history
	
	def act(self, env):
		state = env.state
		turn = state.turn
		dev = self.device
		
		actions = []
		afterstates = []
		won = False
		best_action = None
		
		for a in state.build_actions():
			afterstate = state.clone()
			afterstate.step_mutate(a)
			if afterstate.turn is None:
				won = True
				best_action = a
				break
			afterstates.append(afterstate)
			actions.append(a)
		
		if won:
			value = 1
		else:
			values_logit = self.model(*(x.to(dev) for x in create_batch(afterstates))).detach().cpu().numpy()
			# min because afterstates are the other player's turn
			min_index = np.argmin(values_logit)
			value = 1 - expit(values_logit[min_index])
			best_action = actions[min_index]
		if self.history is not None:
			self.history.append((state.clone(), value))
		
		return best_action

def create_batch(states):
	ds = [encode_state(state) for state in states]
	dl = DataLoader(ds, batch_size = len(ds))
	for batch in dl:
		return batch
	assert False, "shouldn't get here"

class StateValueModel(nn.Module):
	def __init__(self, *, card_embedding_size = 32, board_embedding_size = 32):
		super().__init__()
		
		self.card_embedding_size = card_embedding_size
		self.board_embedding_size = board_embedding_size
		
		self.card_embedding = nn.Embedding(1+len(CARDS), card_embedding_size)
		self.board_embedding = BoardEmbedding(board_embedding_size)
		self.fc1 = nn.Linear(board_embedding_size + 3 * card_embedding_size, 64)
		self.fc2 = nn.Linear(64, 32)
		self.fc3 = nn.Linear(32, 1)
	
	def forward(self, board, mc0, mc1, uc, oc0, oc1):
		"""
			Expects input to be standardized so that the current player is red (i.e. it's always turn 0).
			Predicts the probability current player will win: [0, 1].
		"""
		
		emb_board = self.board_embedding(board)
		emb_mc0 = self.card_embedding(mc0)
		emb_mc1 = self.card_embedding(mc1)
		emb_uc  = self.card_embedding(uc)
		emb_oc0 = self.card_embedding(oc0)
		emb_oc1 = self.card_embedding(oc1)
		
		emb_mc = emb_mc0 + emb_mc1
		emb_oc = emb_oc0 + emb_oc1
		
		out = torch.cat([emb_board, emb_mc, emb_uc, emb_oc], dim = 1)
		out = F.relu(self.fc1(out))
		out = F.relu(self.fc2(out))
		out = self.fc3(out)
		out = out.view(-1)
		
		return out

class BoardEmbedding(nn.Module):
	def __init__(self, embedding_size = 32):
		super().__init__()
		self.embedding_size = embedding_size
		self.conv1 = nn.Conv2d( 6, 32, 1)
		self.conv2 = nn.Conv2d(32, 32, 1)
		self.conv3 = nn.Conv2d(32, 32, 3)
		self.conv4 = nn.Conv2d(32, embedding_size, 3)
	
	def forward(self, board):
		out = board
		out = F.relu(self.conv1(out))
		out = F.relu(self.conv2(out))
		out = F.relu(self.conv3(out))
		out = F.relu(self.conv4(out))
		out = F.adaptive_avg_pool2d(out, 1)
		out = out.view(out.shape[0], self.embedding_size)
		return out

if __name__ == '__main__':
	main()
